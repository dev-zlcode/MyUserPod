//
//  ViewController.m
//  Example
//
//  Created by zhanglei on 2019/1/17.
//  Copyright © 2019 zhanglei. All rights reserved.
//

#import "ViewController.h"
#import <MyUserPod/MyUserPod.h>

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UserModel *model = [UserModel new];
    NSLog(@"%@", model);
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [super touchesBegan:touches withEvent:event];
    
    UserViewController *vc = [UserViewController new];
    [self.navigationController pushViewController:vc animated:YES];
}

@end
