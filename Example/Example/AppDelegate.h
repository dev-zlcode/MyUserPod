//
//  AppDelegate.h
//  Example
//
//  Created by zhanglei on 2019/1/17.
//  Copyright © 2019 zhanglei. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

