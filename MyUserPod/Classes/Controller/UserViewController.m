//
//  UserViewController.m
//  MBProgressHUD
//
//  Created by zhanglei on 2019/1/17.
//

#import "UserViewController.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import <Masonry/Masonry.h>

@interface UserViewController ()

@end

@implementation UserViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    UIImageView *imgView = [UIImageView new];
    imgView.image = [UIImage imageNamed:@"player"];
    [self.view addSubview:imgView];
    
    [imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.centerY.equalTo(self.view);
        make.width.height.mas_equalTo(50);
    }];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [super touchesBegan:touches withEvent:event];
    
    [SVProgressHUD showWithStatus:@"loading..."];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [SVProgressHUD dismiss];
    });
}

@end
